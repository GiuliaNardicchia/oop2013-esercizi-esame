package esami2013.appello05.e3;

import java.util.*;

/* 
 * Rendere questi metodi generici!
 * Esempio:
 * 
 * <X> void print(List<X> l);
 */

public interface ListFunctions {
	
	
	boolean all(List l, Function f); 
	List map(List l,Function f);
	List reduce(List l,Function f);

}
